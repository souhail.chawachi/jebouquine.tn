package tn.insat.jebouquine.data.repository.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tn.insat.jebouquine.data.repository.test.*;


/**
 * Created by Devcartha on 12/7/2015.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({/*TestFindOuvrageByIsbn.class,TestFindOuvrageByEditeur.class,
TestFindOuvrageByDateParution.class,TestFindOuvrageByCategorie.class,
TestFindOuvrageByAuteurNom.class, TestFindOuvrageByAuteurNationalite.class, TestFindOuvrageByChapitre.class*/
TestFindOuvrageByMotCle.class
})
public class RechercheAvanceeTest {

}
