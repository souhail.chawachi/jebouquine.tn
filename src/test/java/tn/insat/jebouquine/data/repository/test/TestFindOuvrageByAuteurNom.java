package tn.insat.jebouquine.data.repository.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import tn.insat.jebouquine.configuration.JeBouquineApplication;
import tn.insat.jebouquine.data.entity.Ouvrage;
import tn.insat.jebouquine.data.repository.IOuvrageRepository;

import java.util.ArrayList;

/**
 * Created by Devcartha on 12/7/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JeBouquineApplication.class)
public class TestFindOuvrageByAuteurNom{
    @Autowired
    IOuvrageRepository ouvrageRepository;
    @Test
    @Transactional
    public void testFindOuvrageByAuteurNomOrPrenom() throws Exception {
        ArrayList<Ouvrage> list = (ArrayList<Ouvrage>) ouvrageRepository.findOuvrageByAuteursNom("chaouechi");
        for (Ouvrage o : list)
            System.out.println(o);
    }
}
